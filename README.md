Auto Flow Actions
=====================

- Move the task to another column once a due or start date is reached
- Automatically set the task due or start date when it is created on a specified column
- Automatically set the task due or start date when it is moved between specified columns
- Add a comment when a task is moved between columns original automatic action does not log when a task is moved using jsonrpc or by another automatic action

Author
------

- Rafael de Camargo
- Based on following plugins:
  - [Move cards to specific columns when due date passed](https://github.com/dmorlitz/kanboard-TaskMoveOnDueDate) by David Morlitz
  - [Push out due date on cards when moving to specific columns](https://github.com/dmorlitz/kanboard-TaskPushDate) by David Morlitz
  - [Add a comment log when moving the task between columns (not a plugin)](https://github.com/kanboard/kanboard/blob/master/app/Action/CommentCreationMoveTaskColumn.php) by Oren Ben-Kiki
- License MIT

Requirements
------------

- Kanboard >= 1.0.44

Installation
------------

You have the choice between 2 methods:

1. Download the zip file and decompress everything under the directory `plugins/AutoFlowActionsPlugins`
2. Clone this repository into the folder `plugins/AutoFlowActionsPlugins`

Note: Plugin folder is case-sensitive.

Quick help
----------

**TaskMoveColumnOnDateWithParams**: Move the task to another column once a due or start date is reached
-------------------------------------------------------------------------------------------------------

Executed when: [cronjob](https://docs.kanboard.org/en/latest/admin_guide/cronjob.html)

Parameters:
- Source column: Which column to move tasks from
- Destination column: Where the tasks will be moved to
- Date field: Which field to considerate when processing tasks
 - Due Date: Move task from *source column* to *destination column* when *due date* is reached
 - Start Date: Move task from *source column* to *destination column* when *start date* is reached
- Weekdays to skip: Do not move the task if the current weekday is one of the following defined in this field
 - Possible values: `sun`, `mon`, `tue`, `wed`, `thu`, `fri`, `sat`
 - Leave it blank to not consider it
 - Usage: If you don't want tasks moved automaticaly on saturdays and sundays, for instance.

**TaskPushDateWhenCreatedWithParams**: Automatically set the task due or start date when it is created on a specified column
----------------------------------------------------------------------------------------------------------------------------

Executed when: task creation

Parameters:
- Column: Execute only for tasks created on this column
- Category: Execute only for tasks having this category (or having none)
- Duration: How much to add to the tasks date field (due or start date, set below)
- Measurement: Sets the duration measurement (hours or days)
- Source date: Which date to base from (Due date set, Start date set or Action date - current time)
- Field to set: Which field will be set
- Weekdays to skip: Skip these days. If the generated date is on these days, it will add **1 day** until it's not a "skip" day
 - Possible values: `sun`, `mon`, `tue`, `wed`, `thu`, `fri`, `sat`
 - Leave ir blank to not consider it
 - Usage: If you set Duration `12`, Measurement `hours`, Source date `Action date`, Field to set `due date`, Weekdays to skip `sat,sun` and create a task on a friday 8PM, the task will end up with a due date to Monday 8PM.

This action makes more sense with days, not with hours. I use it with Recurrent tasks with predefined due dates.

**TaskPushDateWhenMovedWithParams**: Automatically set the task due or start date when it is moved between specified columns
----------------------------------------------------------------------------------------------------------------------------
Executed when: task moved

It works the same ways as **TaskPushDateWhenCreatedWithParams**, but instead you have to select a **source** and **destination** column, and does not expect a category. When a task is moved from the **source** column to the **destination** column, it will set the due/start date according to the logic above.

**CommentCreationMoveTaskColumnIncludeAPI**: Add a comment log when moving the task between columns including automatic events
------------------------------------------------------------------------------------------------------------------------------

Just add comments to a task everytime it's moved. Just like the embedded automatic action, but it also add comments when a task is moved by a automatic action or by the jsonrpc user. The embedded automatic action only logs if the action is being performed by an actual user.
