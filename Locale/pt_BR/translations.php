<?php

return array(
    'AutoFlow: Automatically set the task due or start date based on category when it is created on a specified column' => 'AutoFlow: Definir automaticamente a data fim ou de início da tarefa baseado na categoria quando criada em uma coluna específica',
    'AutoFlow: Automatically set the task due or start date when it is moved between specified columns' => 'AutoFlow: Definir automaticamente a data fim ou de início da tarefa quando movida entre colunas específicas',
    'Duration' => 'Duração',
    'AutoFlow: Move the task to another column once a due or start date is reached' => 'AutoFlow: Mover a tarefa para uma coluna quando chegar a data fim ou de início desta tarefa',
    'AutoFlow: Add a comment log when moving the task between columns including automatic events' => 'AutoFlow: Adicionar um comentário de log quando uma tarefa é movida para uma outra coluna incluindo ações automáticas',
    'Set of automatic actions that handle card flows and dates' => 'Conjunto de ações automáticas para lidar com fluxo de tarefas e datas',
    'Weekdays to skip, comma-separated english abbreviated (3 chars). Max 6 days. Eg: sat,sun,mon' => 'Dias da semana para ignorar, separar por vírgula, abreviado em inglês (3 letras). Máx. 6 dias. Ex: sat,sun,mon',
    'Weekdays when tasks will not be moved, comma-separated english abbreviated (3 chars). Max 6 days. Eg: sat,sun,mon' => 'Dias da semana nos quais a tarefa não deve ser movimentada, separar por vírgula, abreviado em inglês (3 letras). Máx. 6 dias. Ex: sat,sun,mon',
    'Duration' => 'Duração',
    'Measurement' => 'Unidade de medida',
    'Source date' => 'Data a considerar',
    'Field to set' => 'Campo a atualizar',
    'months' => 'meses',
    'Support - Open a ticket' => 'Suporte - Abrir chamado'
);
