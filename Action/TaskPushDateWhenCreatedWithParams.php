<?php

namespace Kanboard\Plugin\AutoFlowActionsPlugins\Action;

use Kanboard\Model\TaskModel;
use Kanboard\Action\Base;

/**
 * Rename Task Title
 *
 * @package action
 * @author  David Morlitz
 */
class TaskPushDateWhenCreatedWithParams extends TaskPushDateWhenMovedWithParams
{

    /**
     * Get automatic action description
     *
     * @access public
     * @return string
     */
    public function getDescription()
    {
        return t('AutoFlow: Automatically set the task due or start date based on category when it is created on a specified column');
    }

    /**
     * Get the list of compatible events
     *
     * @access public
     * @return array
     */
    public function getCompatibleEvents()
    {
        return array(
            TaskModel::EVENT_CREATE,
        );
    }

    /**
     * Get the required parameter for the action (defined by the user)
     *
     * @access public
     * @return array
     */
    public function getActionRequiredParameters()
    {
        return array(
            'column_id' => t('Column'),
            'category_id' => t('Category'),
            'duration' => t('Duration'),
            'measurement' => [
                'hours' => t('hours'),
                'days'  => t('days'),
                'months'=> t('months')
            ],
            'source date' => [
                'date_due' => t('Due Date'),
                'date_started' => t('Start Date'),
                'date_creation' => t('Action date')
            ],
            'field to set' => [
                'date_due' => t('Due Date'),
                'date_started' => t('Start Date'),
            ],
            'weekdays_skip' => t('Weekdays to skip, comma-separated english abbreviated (3 chars). Max 6 days. Eg: sat,sun,mon')
        );
    }

    /**
     * Get the required parameter for the event
     *
     * @access public
     * @return string[]
     */
    public function getEventRequiredParameters()
    {
        return array(
            'task_id',
            // 'task' => array(
            //     'date_due',
            //     'date_started',
            //     'date_creation',
            //     'column_id',
            // ),
        );
    }

    /**
     * Check if the event data meet the action condition
     *
     * @access public
     * @param  array   $data   Event data dictionary
     * @return bool
     */
    public function hasRequiredCondition(array $data)
    {
        $field = $this->getParam('source date');
        $categoryId = $this->getParam('category_id');
        return $data['task']['column_id'] == $this->getParam('column_id') &&
            isset($data['task']['category_id']) && $data['task']['category_id'] == $categoryId &&
            isset($data['task'][$field]) && $data['task'][$field] > 0;
    }

    //inherits below from parent *TaskPushDateWhenMovedWithParams
    //public function doAction(array $data)
}
