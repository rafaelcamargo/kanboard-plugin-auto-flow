<?php

namespace Kanboard\Plugin\AutoFlowActionsPlugins\Action;

use Kanboard\Model\TaskModel;
use Kanboard\Action\Base;

/**
 * Move a task to another column once a predefined start date is reached
 *
 * @package Kanboard\Plugin\AutoFlowActionsPlugins\Action
 * @author  Rafael de Camargo
 */
class TaskMoveColumnOnDateWithParams extends Base
{
    protected $weekdays = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];

    /**
     * Get action description
     *
     * @access public
     * @return string
     */
    public function getDescription()
    {
        return t('AutoFlow: Move the task to another column once a due or start date is reached');
    }

    /**
     * Get the list of compatible events
     *
     * @access public
     * @return array
     */
    public function getCompatibleEvents()
    {
        return array(TaskModel::EVENT_DAILY_CRONJOB);
    }

    /**
     * Get the required parameter for the action (defined by the user)
     *
     * @access public
     * @return array
     */
    public function getActionRequiredParameters()
    {
        return array(
            'src_column_id' => t('Source column'),
            'dest_column_id' => t('Destination column'),
            'date field' => [
                'date_due' => t('Due Date'),
                'date_started' => t('Start Date')
            ],
            'weekdays_skip' => t('Weekdays when tasks will not be moved, comma-separated english abbreviated (3 chars). Max 6 days. Eg: sat,sun,mon')
        );
    }

    /**
     * Get the required parameter for the event
     *
     * @access public
     * @return string[]
     */
    public function getEventRequiredParameters()
    {
        return array('tasks');
    }

    /**
     * Execute the action (close the task)
     *
     * @access public
     * @param  array   $data   Event data dictionary
     * @return bool            True if the action was executed or false when not executed
     */
    public function doAction(array $data)
    {
        $results = array();

        $field = $this->getParam('date field');
        $taskPositions = [];
        $taskById = [];
        foreach ($data['tasks'] as $task) {
            if ($task[$field] > 0 && $task[$field] <= time() && $task['column_id'] == $this->getParam('src_column_id')) {
		// add task to array with position to be sorted after
                // its not guaranteed position is unique
                $taskPositions[$task['id']] = $task['position'];
                $taskById[$task['id']] = $task;
            }
        }
        asort($taskPositions);
        foreach ($taskPositions as $id => $position) {
            $task = $taskById[$id]; 
            $results[] = $this->taskPositionModel->movePosition(
                $task['project_id'],
                $task['id'],
                $this->getParam('dest_column_id'),
                $position,
                $task['swimlane_id'],
                true
            );
        }

        return in_array(true, $results, true);
    }

    /**
     * Check if the event data meet the action condition
     *
     * @access public
     * @param  array   $data   Event data dictionary
     * @return bool
     */
    public function hasRequiredCondition(array $data)
    {
        $weekdays_skip = explode(',', strtolower($this->getParam('weekdays_skip')),6);
        $date_info = getdate();
        $date_weekday = $this->weekdays[$date_info['wday']];
        return count($data['tasks']) > 0 && !in_array($date_weekday, $weekdays_skip);
    }
}
