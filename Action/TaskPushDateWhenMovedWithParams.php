<?php

namespace Kanboard\Plugin\AutoFlowActionsPlugins\Action;

use Kanboard\Model\TaskModel;
use Kanboard\Action\Base;

/**
 * Rename Task Title
 *
 * @package action
 * @author  David Morlitz
 */
class TaskPushDateWhenMovedWithParams extends Base
{

    protected $weekdays = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
    /**
     * Get automatic action description
     *
     * @access public
     * @return string
     */
    public function getDescription()
    {
        return t('AutoFlow: Automatically set the task due or start date when it is moved between specified columns');
    }

    /**
     * Get the list of compatible events
     *
     * @access public
     * @return array
     */
    public function getCompatibleEvents()
    {
        return array(
            TaskModel::EVENT_MOVE_COLUMN,
        );
    }

    /**
     * Get the required parameter for the action (defined by the user)
     *
     * @access public
     * @return array
     */
    public function getActionRequiredParameters()
    {
        return array(
            'src_column_id' => t('Source column'),
            'dest_column_id' => t('Destination column'),
            'duration' => t('Duration'),
            'measurement' => [
                'hours' => t('hours'),
                'days'  => t('days'),
                'months'=> t('months')
            ],
            'source date' => [
                'date_due' => t('Due Date'),
                'date_started' => t('Start Date'),
                'date_moved' => t('Action date')
            ],
            'field to set' => [
                'date_due' => t('Due Date'),
                'date_started' => t('Start Date'),
            ],
            'weekdays_skip' => t('Weekdays to skip, comma-separated english abbreviated (3 chars). Max 6 days. Eg: sat,sun,mon')
        );
    }

    /**
     * Get the required parameter for the event
     *
     * @access public
     * @return string[]
     */
    public function getEventRequiredParameters()
    {
        return array(
            'task_id',
    //        'task' => array(
    //            'date_due',
    //            'date_started',
    //            'date_moved',
    //        ),
            'changes' => array(
                'src_column_id',
                'dst_column_id',
            ),
        );
    }

    /**
     * Check if the event data meet the action condition
     *
     * @access public
     * @param  array   $data   Event data dictionary
     * @return bool
     */
    public function hasRequiredCondition(array $data)
    {
        $field = $this->getParam('source date');
        return $data['changes']['src_column_id'] == $this->getParam('src_column_id') &&
            $data['changes']['dst_column_id'] == $this->getParam('dest_column_id') &&
            isset($data['task'][$field]) && $data['task'][$field] > 0;
    }

    /**
     * Execute the action
     *
     * @access public
     * @param  array   $data   Event data dictionary
     * @return bool            True if the action was executed or false when not executed
     */
    public function doAction(array $data)
    {
        $weekdays_skip = explode(',', strtolower($this->getParam('weekdays_skip')),6);
        $timediff = '+'.$this->getParam('duration').' '.$this->getParam('measurement');
        $new_date = $data['task'][$this->getParam('source date')];
        // add required amount of time to task
        $new_date = strtotime($timediff, $new_date);
        // if new_date is on a weekday to skip, keep adding 1 day until it is not
        while (in_array($this->getWeekdate($new_date), $weekdays_skip)) {
            $new_date = strtotime("+1 day", $new_date);
        }
        $values = array(
            'id' => $data['task_id'],
            $this->getParam('field to set') => $new_date,
        );

        return $this->taskModificationModel->update($values, true);
    }

    protected function getWeekdate($date)
    {
        $date_info = getdate($date);
        return $this->weekdays[$date_info['wday']];
    }
}
