<?php
namespace Kanboard\Plugin\AutoFlowActionsPlugins;
use Kanboard\Core\Plugin\Base;
use Kanboard\Core\Translator;
use Kanboard\Plugin\AutoFlowActionsPlugins\Action\TaskMoveColumnOnDateWithParams;
use Kanboard\Plugin\AutoFlowActionsPlugins\Action\TaskPushDateWhenMovedWithParams;
use Kanboard\Plugin\AutoFlowActionsPlugins\Action\TaskPushDateWhenCreatedWithParams;
use Kanboard\Plugin\AutoFlowActionsPlugins\Action\CommentCreationMoveTaskColumnIncludeAPI;
class Plugin extends Base
{
    public function initialize()
    {
        $this->actionManager->register(new TaskMoveColumnOnDateWithParams($this->container));
        $this->actionManager->register(new TaskPushDateWhenMovedWithParams($this->container));
        $this->actionManager->register(new TaskPushDateWhenCreatedWithParams($this->container));
        $this->actionManager->register(new CommentCreationMoveTaskColumnIncludeAPI($this->container));
    }

    public function onStartup()
    {
        Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
    }

    public function getPluginName()
    {
        return 'Auto-Flow Actions';
    }

    public function getPluginDescription()
    {
        return t('Set of automatic actions that handle card flows and dates');
    }

    public function getPluginAuthor()
    {
        return 'Rafael de Camargo';
    }

    public function getPluginVersion()
    {
        return '1.2.0';
    }

    public function getPluginHomepage()
    {
        return 'https://gitlab.com/rafaelcamargo/kanboard-plugin-auto-flow';
    }

    public function getCompatibleVersion()
    {
        return '>=1.0.44';
    }
}
